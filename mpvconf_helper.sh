#! /bin/bash

# selector function taken from https://gitlab.com/Hitman_47/utilities.sh, slimmed down
sel() {
selopt=0 elnr=4 color1='45;140;155' color2='245;220;50' ttl_text=" $title "; while true; do cols=$(tput cols)
# prepare text feed, output title and header
echo -e '\e[1K\e[H\e[?7h\e[J\e[?25l\e[1m\e[48;2;'$color1'm'"$(printf %"$cols"s)"'\e['$(( $cols - 10 ))'G'"$(date '+%a %R')"'\e[49m\e['$(( ( $cols - ${#ttl_text} ) / 2 ))'G'$ttl_text'\e[38;2;'$color1'm\e[1E\e[2K'$header'\e[0m\e[K'
# output the options
seq 0 "$elnr" | while read i; do [ "$i" = "$selopt" ] && bef='\e[1;38;2;'$color2';48;2;'$color1'm' aft='\e[0m' || unset bef aft; echo -e "\e[1K$bef${menu[$i]}$aft\e[K"; done
# make the selection
read -s -n3 key
case $key in
	$'\x1b[B') [ $selopt -ne $elnr ] && let selopt=selopt+1;; # up
	$'\x1b[A') [ $selopt -ne 0 ] && let selopt=selopt-1;; # down
	""|$'\x1b[C') opt="${menu["$selopt"]}"; echo -e "\e[H\e[J\e[?25h"; return 0;; # select (Enter/right)
	*) break
esac; done
}

lang=en title='mpv configuration helper by Hitman' header='Select an option, note that the config will be overwritten:'
[[ -d "/tmp/mpv_config" ]] && rm -rf "/tmp/mpv_config"; cd /tmp && git clone https://gitlab.com/Hitman_47/mpv_config

while true; do
menu=( "-> High end GPU (high res display)" "-> Midrange GPU (high res display)" "-> Midrange GPU (<=1080p display)" "-> Low end GPU" "-> Change yt-dlp lang: $lang" )
sel

prep() { cd ~/.config && rm -r mpv && cp -rv "/tmp/mpv_config/mpv" ~/.config && cd mpv || exit 1
[[ $lang != en ]] && sed -i "s/sub\-lang\=\".*\./sub\-lang\=\"$lang\./g" mpv.conf
}

case $opt in
*High*)
	prep
	sed -i 's/ewa_lanczos/ewa_lanczos4sharpest/' mpv.conf || err=1
	sed -i 's/C4F16/C4F32/g' mpv.conf || err=1
	sed -i '39b1; b ;:1;s/FSR.*glsl/ArtCNN_C4F16_DS.glsl/' mpv.conf || err=1
	sed -i 's/FastBilateral/CfL_Prediction/g' mpv.conf || err=1
	sed -i '36,41d' mpv.conf || err=1
;;

*Mid*high*)
	prep # the default config is mid+highres, only the other options will edit mpv.conf
;;

*Mid*1080*)
	prep
	sed -i '39b1; b ;:1;s/^.*$/glsl-shaders\=\"\~\~\/shaders\/nlmeans_temporal_mid_luma\.glsl\"/g' mpv.conf || err=1
	sed -i '51b1; b ;:1;s/^.*$/glsl-shaders\=\"\~\~\/shaders\/CfL_Prediction_Lite\.glsl\"/g' mpv.conf || err=1
	sed -i '34d; 52d; 53,57d' mpv.conf || err=1
;;

*Low*)
	prep
	sed -i 's/CfL.*glsl/FastBilateral.glsl/g' mpv.conf || err=1
	sed -i '6,9d; 27d; 34,35d; 39d; 42,57d; 71d' mpv.conf || err=1
;;

*lang*)
	read -p 'Input language for yt-dlp: ' lang; continue
;;
*) exit
esac
[[ -z $err ]] && echo Done || echo Error; break
done
