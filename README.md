Simple and easy to understand gpu-next configuration for mpv - with temporal denoising and upscaling, proper hdr->sdr rendering, image/audio modes, discrete scripts and more!

It's aimed primarily for a 1440/4k display but the helper script can adjust mpv.conf to suit a lower resolution display.

*A special thanks to Ethan/AN3223 for the support in regards to the NLMeans shaders.
