mp.observe_property('pause', 'bool', function(_, paused)
    mp.add_timeout(0.1, function()
        if paused then mp.command("script-message osc-visibility always")
        else mp.command("script-message osc-visibility auto") end
    end)
end)
